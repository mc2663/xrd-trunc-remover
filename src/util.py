class cfg():

    def __init__(self):
        self.ORIGINAL_WIDTH = 1065 
        self.ORIGINAL_HEIGHT = 1030 
        self.REDUCED_WIDTH = 213 
        self.REDUCED_HEIGHT = 206 


def fn_parser(fn):
    conditions = fn.split('_')[1:]

    cond_names = ['x', 'y', 'tpeak', 'dwell']
    cond_dict = {name: int(c) for name, c in zip(cond_names, conditions)}

    return cond_dict


if __name__ == '__main__':
    print(fn_parser('chess_-26_+10_+01329_+2678'))
