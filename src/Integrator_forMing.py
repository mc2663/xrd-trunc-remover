import numpy as np
import matplotlib.pyplot as plt
from pyFAI.azimuthalIntegrator import AzimuthalIntegrator as az
import pyFAI
from xrdCalibFit import PoniFit, automask, ptt, Mask
import sys
import h5py as h5
import glob
#from SARA_cornell_funcs import *
from pathlib import Path
import cv2

def ZingerBGone(data,mini=0, maxi=9000, fill=0):
    """returns filtered data"""
    data = np.where((mini<=data) & (data<maxi),data,fill)
    return data

def adaptive_thresh(data, dim, mode='median'):
    mode_dict = {'median': median_thresh, 
                 'mean': mean_thresh,
                 'minmax': minmax_thresh}
    result = np.zeros(data.shape)
    for i in range(int(data.shape[0]/dim)):
        for j in range(int(data.shape[1]/dim)):
            if (i+1)*dim > data.shape[0] and (j+1)*dim > data.shape[0]:
                result[i*dim:, j*dim:] = mode_dict[mod](data[i*dim:, j*dim:])
            elif (i+1)*dim > data.shape[0]:
                result[i*dim:, j*dim:(j+1)*dim] =\
                                    mode_dict[mode](data[i*dim:, j*dim:(j+1)*dim]) 
            elif (j+1)*dim > data.shape[0]:
                 result[i*dim:(i+1)*dim, j*dim:] =\
                                    mode_dict[mode](data[i*dim:(i+1)*dim, j*dim:])
            else:
                result[i*dim:(i+1)*dim, j*dim:(j+1)*dim] =\
                                    mode_dict[mode](data[i*dim:(i+1)*dim, j*dim:(j+1)*dim])
    result = result.astype('int')
    return result


def median_thresh(data_block):
    temp = data_block.reshape(-1)
    thresh = np.sort(temp)[int(temp.shape[0]/2)]
    return data_block > thresh+1

def mean_thresh(data_block):
    return data_block > np.mean(data_block)

def minmax_thresh(data_block):
    thresh = (np.max(data_block)+np.min(data_block))/2
    return data_block > thesh


ponis = glob.glob('/home/mingchiang/Desktop/Data/06_60871_Y-Pd-O/Calibration/PONIs/*.poni')
print(ponis)
pf = PoniFit(ponis)
eiger1M = pyFAI.detectors.Detector(
            pixel1=75.0e-6, pixel2=75.0e-6, max_shape=(1065,1030)
        )

ai = az(*pf.getpars((40,5)), detector=eiger1M, wavelength=1.2781875567010312e-10)




frame = '/home/mingchiang/Desktop/Data/06_60871_Y-Pd-O/Stripes/chess_-44_-10_+00900_+1215/frame_001/'
files = []
integrated = []
for idx, file in enumerate(sorted(glob.glob(frame+'*.h5'))):
    files.append(file)
    print(idx)
    if 'master' not in file and idx>120:
        im = ZingerBGone(h5.File(file,'r')['entry']['data']['data'][0],maxi=15)
        #im = adaptive_thresh(im, 10) 
        #plt.imshow(im)
        #plt.show()
        ai.mask = Mask(im, maxi=100)
        
        Q, I1d = ai.integrate1d(im, 360, unit="q_nm^-1", method="csr", safe=True)
        integrated.append(I1d)

integrated = np.array(integrated)
plt.imshow(integrated)
plt.show()
#np.save(f'/home/mingchiang/Desktop/{cond}.npy', integrated)

