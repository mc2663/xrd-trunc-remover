''' Truncation Removal module for 2D XRD data '''
# pylint: disable=E1101
from multiprocessing import Pool
from functools import partial

from scipy.optimize import leastsq
import matplotlib.pyplot as plt
import numpy as np
import cv2

np.seterr(divide='ignore')

# TODO Parallel loading?
# TODO linting and docs
# TODO save raw as npy?

def remove_xrd_trunc(data, cfg, mode='extend', start_intensity=4, window=3,
                     max_change=0.2,  plot=False):
    ''' function for removing points that lies outside of linear-log distribution'''
    count = []
    resized_data = cv2.resize(data, (cfg.REDUCED_HEIGHT, cfg.REDUCED_WIDTH)) 

    for i in range(int(np.max(resized_data))):
        count.append(np.sum(resized_data == i))
    count = np.array(count)

    turning_point, slope = detect_linearity(np.log(count[start_intensity:]),
                                            window=window, max_change=max_change)
    turning_point += start_intensity

    if mode == 'extend':
        threshold = extend_linearly_to_zero(turning_point, np.log(count), slope)
    else:
        threshold = turning_point

    if plot:
        plot_trunc(count, threshold)
    
    data[data > threshold] = threshold
    return data

def parrellized_xrd_trunc_removal(data_arr, cfg, mode='extend', start_intensity=4, window=3,
                     max_change=0.2,  plot=False):
    '''
    Parraellized trunction removeal function
    Maybe add processor count
    '''
    partial_func = partial(remove_xrd_trunc, cfg=cfg, mode=mode, start_intensity=start_intensity,
                           window=window, max_change=max_change, plot=plot)
    with Pool() as pool:
        data_arr = pool.map(partial_func, data_arr)
    return data_arr

def extend_linearly_to_zero(x, y, slope):
    ''' Extend a point linearly to 0 with given slope '''
    assert isinstance(x, int), "x has to be an integer"
    return int(x - y[x]/slope)

def plot_trunc(count, threshold):
    ''' Plotting trunction rod removal details '''
    upper = count.shape[0] if (2*threshold > count.shape[0]) else 2*threshold
    plt.plot(count[:upper])
    plt.xlabel('Intensity')
    plt.ylabel('Counts')
    plt.yscale('log')
    plt.axvline(x=threshold, c='r', linestyle="dashed")
    plt.ylim(10**-1, 10**6)
    plt.show()

def coarse_grain(arr, scale):
    '''
    Lowering the dimension of a mask using by coarse-graining with the rounded average
    of nxn kernel
    '''
    # TODO: Padding options
    new_arr = np.zeros((int(arr.shape[0]/scale), int(arr.shape[1]/scale)))
    for i in range(new_arr.shape[0]):
        for j in range(new_arr.shape[1]):
            new_arr[i,j] = np.sum(arr[scale*i:(i+1)*scale, scale*j:(j+1)*scale]) / scale**2
    return new_arr

#def fine_grain(arr, new_arr_shape):
#    ''' Get higher resolution with coarse-grained masks '''
#    return

def adaptive_thresh(data, dim, mode='median'):
    '''
    Adaptive thresholding with 3 modes. The process is always done in the unit of blocks,
    the size of which is defined by the dim parameter (square)

    Modes:
        1. median: for each block, set the threshold to be the median + 1
        2. mean: for each block, set the thrshold to the mean
        3: minmax: for each block, set the threshold to be (min+max)/2

    Return (1, 0) mask of the same size
    '''
    mode_dict = {'median': median_thresh,
                 'mean': mean_thresh,
                 'minmax': minmax_thresh}
    result = np.zeros(data.shape)
    for i in range(data.shape[0]//dim):
        for j in range(data.shape[1]//dim):
            if (i+1)*dim > data.shape[0] and (j+1)*dim > data.shape[0]:
                result[i*dim:, j*dim:] = mode_dict[mode](data[i*dim:, j*dim:])
            elif (i+1)*dim > data.shape[0]:
                result[i*dim:, j*dim:(j+1)*dim] =\
                                    mode_dict[mode](data[i*dim:, j*dim:(j+1)*dim])
            elif (j+1)*dim > data.shape[0]:
                result[i*dim:(i+1)*dim, j*dim:] =\
                                    mode_dict[mode](data[i*dim:(i+1)*dim, j*dim:])
            else:
                result[i*dim:(i+1)*dim, j*dim:(j+1)*dim] =\
                                    mode_dict[mode](data[i*dim:(i+1)*dim, j*dim:(j+1)*dim])
    result = result.astype('int')
    return result

def median_thresh(data_block):
    ''' Thresholding the data_block with (median+1) '''
    temp = data_block.reshape(-1)
    thresh = np.sort(temp)[int(temp.shape[0]/2)]
    return data_block > thresh+1

def mean_thresh(data_block):
    ''' Thresholding the data_block with mean '''
    return data_block > np.mean(data_block)

def minmax_thresh(data_block):
    ''' Thresholding the data_block with (min+max)/2'''
    thresh = (np.max(data_block)+np.min(data_block))/2
    return data_block > thresh

def detect_linearity(data, max_change=0.15, window=3, freq_threshold=4.6):
    ''' Detecting linearity with sliding differentiating 'window' amount of data point'''
    slope = 0
    for i in range(data.shape[0]-window):
        fit_param = fit_linear(data[i:i+window])
        fitted_slope = fit_param[0]
        if data[i] < freq_threshold:
            return i, fitted_slope
        if i==0:
            slope = fitted_slope
        elif (slope - fitted_slope)/slope > max_change:
            return i, fitted_slope
    return int(np.max(data)), -1

def fit_linear(d):
    ''' Simple function for fitting d linearly '''
    def f(a):
        return a[0]*np.linspace(1, d.shape[0], d.shape[0])+a[1]-d
    fit_param, message = leastsq(f, [0,0])
    if message not in [1,2,3,4]:
        print('Fitting error')
    else:
        return fit_param


############## opencv ######################
#kernel = np.ones((5,5), np.float32)/25
#data=data.astype(np.float32)
#smoothed = cv2.filter2D(data, -1, kernel)
#
#threshold = cv2.adaptiveThreshold(smoothed.astype('uint8'), 1, cv2.ADAPTIVE_THRESH_GAUSSIAN_C,\
#                                 cv2.THRESH_BINARY_INV,11,2)
#fig, axs = plt.subplots(2)
#axs[0].imshow(threshold, vmin=0, vmax=5)
#axs[1].imshow(data, vmin=0, vmax=5)
#plt.show()

############## Direct threshold? ############
#fig, axs = plt.subplots(2)
#axs[0].imshow(data, vmin=0, vmax=5)
#data[data>10]=0

#axs[1].imshow(data, vmin=0, vmax=5)
#plt.show()

############## Kernel Map ##################
#kernel = np.tile(np.array([-1, -1, 1, 1, 1, 1, -1, -1]), (8,1)).astype(np.float32)
#data = data[:1030, :].astype(np.float32)
#z = np.fft.irfft2(np.fft.rfft2(data) * np.fft.rfft2(kernel, data.shape))
#print(z.shape)
#plt.imshow(z, vmin=-64, vmax=64)
#plt.show()
