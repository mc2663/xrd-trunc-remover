import gzip
import h5py

def gz_to_h5(fname):
    h = 0
    f = gzip.open(fname, 'rb') 
    h = h5py.File(f)
    return h


