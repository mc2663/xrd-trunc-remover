import numpy as np
import matplotlib.pyplot as plt
from copy import deepcopy, copy
from numpy.linalg import norm

# A implementation for TODO: reference here

def conical_NMF(X,rank):
    R = deepcopy(X)
    A = []
    p = np.ones(X.shape[0])
    while len(A) < rank:
        j = detection(R,X,p)
        #print('detected: ',j)
        if j not in A:
            A.append(j)
        H = projection(X, A, 100)
        W = X[:,A]
        R = X-W.dot(H)
    return W, H, A, R


def detection(R, X, p):
    #print('Detecting..')
    max_j = 0
    max_obj = 0
    #i = find_max_dist(R, X)
    i = find_max(R)
    #print('Max dist: ',i)
    for j in range(X.shape[1]):
        obj = (R[:,i].T.dot(X[:,j]))/(p.T.dot(X[:,j]))
        if obj > max_obj:
            max_j = j
            max_obj = obj
    return max_j
 
def projection(X, A, max_cycle):
    #print('Projecting..')
    n = X.shape[1]
    r = len(A)
    B = np.random.rand(len(A), X.shape[1])
    C = X.T.dot(X[:,A])
    S = X[:,A].T.dot(X[:,A])
    s = np.diag(S)
    U = B.T.dot(S)
    #print(B.shape, C.shape, S.shape, s.shape, U.shape) 
    cycle = 0
    while(cycle < max_cycle):
        cycle += 1
        #print('Projection cycle {}'.format(cycle))
        for i in range(len(A)):
            b = copy(B[i])
            u = U[:,i]-s[i]*b
            temp = C[:,i]-u
            temp[temp<0] = 0
            B[i] = temp/s[i]
            U = U + np.outer((B[i]-b), S[:,i].T)
        #print(objective(X, r, U, C, B))   
        #plt.plot(B.T)
        #plt.show()
    return B

def objective(X, r, U, C, B):
    s = 0
    for i in range(r):
        s += (U[:,i] + C[:,i]).T.dot(B.T[:,i])
    return norm(X)**2 + s

def find_max(R):
    n = norm(R, axis=0)
    return np.where(n == np.max(n))[0][0]

def find_max_dist(R, X):
    product = R.T.dot(X)
    product[product<0] = 0
    n = norm(product, axis=1)
    return np.where(n == np.max(n))[0][0] 

if __name__ == '__main__':
   
    resampled = np.load('BTO_substrate_subtracted.npy')
    #for i in range(625):
    #    center[i] = center[i]/np.sum(center[i])


    W, H, A, R = conical_NMF(resampled[2].T, 3)
    print(A)
    for i in range(W.shape[1]):
        plt.plot(W[:,i])
        plt.show()





















