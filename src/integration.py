import sys
import glob
from pathlib import Path
import collections

import cv2
import numpy as np
import matplotlib.pyplot as plt
import h5py as h5
import numpy as np
import matplotlib.pyplot as plt
from pyFAI.azimuthalIntegrator import AzimuthalIntegrator as az
import pyFAI

from xrdCalibFit import PoniFit, automask, ptt, Mask

pyFAI_setups = collections.namedtuple('pyFAI_setups', 
              ['DETECTOR_PXL1', 'DETECTOR_PXL2','DETECTOR_SHAPE', 'WAVELENGTH', 'GETPARS'])
pyFAI_const = pyFAI_setups( 
              DETECTOR_PXL1=75.0e-6,
              DETECTOR_PXL2=75.0e-6,
              DETECTOR_SHAPE=(1065, 1030),
              WAVELENGTH=1.2781875567010312e-10,
              GETPARS=(40,5) 
              )

def ZingerBGone(data, mini=0, maxi=9000, fill=0):
    """returns filtered data"""
    data = np.where((mini<=data) & (data<maxi),data,fill)
    return data

def init_integrator(poni_paths):
    pf = PoniFit(poni_paths)

    eiger1M = pyFAI.detectors.Detector(
            pixel1 = pyFAI_const.DETECTOR_PXL1,
            pixel2 = pyFAI_const.DETECTOR_PXL2,
            max_shape = pyFAI_const.DETECTOR_SHAPE
            )

    ai = az(*pf.getpars(pyFAI_const.GETPARS),
            detector = eiger1M,
            wavelength = pyFAI_const.WAVELENGTH
            )
                                       
    return ai

def integrate(integrator, data, zinger_maxi=50, mask_maxi=100, pxls_in_Q=1024):
    #ZingerBGone too aggressive
    im = ZingerBGone(data, maxi=zinger_maxi)
    integrator.mask = Mask(im, maxi=mask_maxi)

    Q, I1d = integrator.integrate1d(im, pxls_in_Q, unit="q_nm^-1", method="cst", safe=True)
    return Q, I1d


if __name__ == '__main__':
    ponis = glob.glob('/home/mingchiang/Desktop/Data/06_60871_Y-Pd-O/Calibration/PONIs/*.poni')
    print(ponis)
    ai = init_integrator(ponis)



    frame = '/home/mingchiang/Desktop/Data/06_60871_Y-Pd-O/Stripes/chess_-44_-10_+00900_+1215/frame_001/'
    files = []
    integrated = []
    for idx, file in enumerate(sorted(glob.glob(frame+'*.h5'))):
        files.append(file)
        print(idx)
        if 'master' not in file and idx>120:
            im = ZingerBGone(h5.File(file,'r')['entry']['data']['data'][0],maxi=15)
            
            ai.mask = Mask(im, maxi=100)
            
            Q, I1d = ai.integrate1d(im, 360, unit="q_nm^-1", method="csr", safe=True)
            integrated.append(I1d)

    integrated = np.array(integrated)
    plt.imshow(integrated)
    plt.show()
    #np.save(f'/home/mingchiang/Desktop/{cond}.npy', integrated)

