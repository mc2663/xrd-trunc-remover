from pathlib import Path
import os
import glob

from tqdm import tqdm
import h5py
import matplotlib.pyplot as plt
import numpy as np

from xrdCalibFit import PoniFit, automask, ptt, Mask
from pyFAI.azimuthalIntegrator import AzimuthalIntegrator as az
from pyFAI.detectors import Detector

def ZingerBGone(data,mini=0, maxi=9000, fill=0):
    """returns filtered data"""
    data = np.where((mini<=data) & (data<maxi),data,fill)
    return data

h5_fn = '/frame_001/frame_EIG1_001_master.h5'

home = Path.home()
path = home / 'Desktop' / 'Data' / '06_60871_Y-Pd-O' / 'Stripes'
path_ls = sorted(list(path.glob('Stripes/*')))
ponis = sorted(list(path.glob('/Calibration/PONIs/*.poni')))

# Instantiating PyFAI objects
pf = PoniFit(ponis)
eiger1M = Detector(pixel1=75.0e-6, pixel2=75.0e-6, max_shape=(1065,1030))
ai = az(*pf.getpars((40,5)), detector=eiger1M, wavelength=1.2781875567010312e-10)

for idx, path in enumerate(path_ls):
    print(f'Woring on {idx+1}/{len(path_ls)}: {os.path.basename(path)}')
    h5path = path + h5_fn

    stripe_data = np.zeros((201, 512, 1030))

    f = h5py.File(h5path, 'r')

    for i in tqdm(range(1,202)):
        data = f['entry']['data'][f'data_{str(i).zfill(6)}'][0][:512,:].astype('int32')
        stripe_data[i-1] = data

    print(f'Max: {np.max(stripe_data)}')
    count = []
    stripe_data = stripe_data.astype('uint16')
    stripe_data[stripe_data > 100] =0 
    for i in tqdm(range(np.max(stripe_data))):
        count.append(np.sum(stripe_data == i))
    
    #fig, axs = plt.subplots(3)
    #axs[0].imshow(np.sum(stripe_data, axis=0), vmax=1000)
    count = np.array(count)

    turning_point = linearity_detector(np.log(count[4:]), max_change=0.2)+4
    stripe_data[stripe_data > turning_point]=turning_point

    total = np.sum(stripe_data, axis=0)
    #axs[1].imshow(total, vmax=1000)
    #axs[2].plot(count)
    #ax = plt.gca()
    #ax.set_yscale('log')
    #plt.show()
 
    ############################## Integration Code #################################
    #frame = f'{path}/frame_001/'
    #files = []
    #integrated = []
    #for idx, file in tqdm(enumerate(sorted(glob.glob(frame+'*.h5')))):
    #    files.append(file)
    #    if 'master' not in file:
    #        im = ZingerBGone(h5py.File(file,'r')['entry']['data']['data'][0],maxi=15)
    #        im[im>turning_point]=turning_point
    #        ai.mask = Mask(im, maxi=100)

    #        Q, I1d= ai.integrate1d(im, 360, unit="q_nm^-1", method="csr", safe=True)
    #        integrated.append(I1d)
    #integrated = np.array(integrated)
    #fig, axs = plt.subplots(2)
    #axs[0].imshow(integrated)
    #
    #integrated = []
    #for idx, file in tqdm(enumerate(sorted(glob.glob(frame+'*.h5')))):
    #    files.append(file)
    #    if 'master' not in file:
    #        im = ZingerBGone(h5py.File(file,'r')['entry']['data']['data'][0],maxi=15)
    #        ai.mask = Mask(im, maxi=100)

    #        Q, I1d= ai.integrate1d(im, 360, unit="q_nm^-1", method="csr", safe=True)
    #        integrated.append(I1d)
    #axs[1].imshow(integrated)
    #plt.savefig(f'{os.path.basename(path)}.png')
