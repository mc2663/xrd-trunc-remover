import h5py
import cv2
import matplotlib.pyplot as plt
import numpy as np
import glob
import os
from tqdm import tqdm

from xrd_trunc_remover import *
from conical_NMF import conical_NMF

path_ls=glob.glob('/home/mingchiang/Desktop/Data/06_60871_Y-Pd-O/Stripes/*')
h5_fn = '/frame_001/frame_EIG1_001_master.h5'

#stripe = np.zeros((200,21012))

#full_data = np.load('test.npy')



for idx, path in enumerate(path_ls[:1]):
    print(f'Woring on {idx+1}/225: {os.path.basename(path)}')
    h5path = path + h5_fn

    stripe_data = np.zeros((200, 102, 206))

    f = h5py.File(h5path, 'r')
    bg = f['entry']['data']['data_000001'][0][:512,:].astype('int32')
    bg[bg>10000]=0

    for i in tqdm(range(2,202)):
        data = f['entry']['data'][f'data_{str(i).zfill(6)}'][0][:512,:].astype('int32')
        data[data>100]=0
        data = data - bg
        data[data<0] = 0
        data = data.astype(np.uint8) 
        #data = cv2.GaussianBlur(data, (5,5), 0)
        stripe_data[i-2] = coarse_grain(data, 5) 
        if i>120:
            plt.imshow(data)
            plt.show()
    stripe_data[stripe_data>255]=255
    
    k = np.mean(stripe_data, axis=0) + np.std(stripe_data, axis=0)

    masks = np.zeros((stripe_data.shape[0], stripe_data.shape[1]*stripe_data.shape[2]))
    for i in range(200):
        mask = np.ravel(stripe_data[i]>k)
        mask = mask.astype(np.float32)
        masks[i] = mask
    
    W, H, _, _ = conical_NMF(masks.T, rank=3)
    
    for i in range(3):
        plt.imshow(W[:,i].reshape(102, 206))
        plt.show()

#for i in range(1,10):
#    data = f['entry']['data'][f'data_{str(i).zfill(6)}'][0][:512,:].astype('int32')
#    data[data>10000]=0
#    #fig, axs = plt.subplots(3)
#    sub = data-bg
#    sub[sub<0]=0
#    #axs[1].imshow(data)
#    #axs[0].imshow(data-bg)
#    #axs[2].imshow(sub)
#    #plt.title(os.path.basename(path))
#    #plt.show()
#
#
#    d = coarse_grain(sub, 5)
#    #plt.imshow(data>5)
#    fig, axs = plt.subplots(2)
#    axs[1].imshow(d>np.mean(d)+0.7*np.std(d))
#    mask = d > (np.mean(d) + 0.7*np.std(d))
#    mask = mask.astype(np.float32)
#    axs[0].imshow(data, vmax=5)
#    #plt.title(os.path.basename(path))
#    plt.show()
#    print(mask.dtype)
#    stripe[i] = np.ravel(mask)

