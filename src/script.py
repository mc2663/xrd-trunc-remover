from pathlib import Path
from copy import deepcopy
from time import time
import gzip

from tqdm import tqdm
import cv2
import numpy as np
import matplotlib.pyplot as plt
import h5py

from util import cfg
from xrd_trunc_remover import remove_xrd_trunc, parrellized_xrd_trunc_removal

config = cfg()
home = Path.home()
path = home / 'Desktop' / 'Data' / 'CHESS2021' / '06_60871_Y-Pd-O'
 
frames = sorted(list(path.glob('Stripes/chess_-10_+00_+00900_+0250/frame_001/*data*')))

data = np.zeros((201, 1065, 1030))
for idx, frame in tqdm(enumerate(frames)):
    with gzip.open(frame, 'r') as f:
        f = h5py.File(f, 'r')
        
        data[idx] = f['entry']['data']['data'][0]
data[data>1000]=0

np.save('test', data)

print("Done reading data")
#data = data.tolist()
print("Done turning data to list")
d = deepcopy(data[125])
np.save("before", d)
start = time()
after_data = parrellized_xrd_trunc_removal(data, config, mode="")
print(time()-start)
np.save("after", after_data[125])
#fig, ax = plt.subplots(2)
#ax[0].imshow(d)
#ax[1].imshow(data[100])
#plt.show()
